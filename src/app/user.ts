export class User {
    user_id: number;
    username: string;
    password: string;
    prenom: string;
    nom: string;
    email: string;
    tel: number;
    dateNaissance: string;
    token?: string;
}