import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { UserService } from "../../user.service";
import { Router } from '@angular/router';
import { Event } from "../../event";
import { AuthService } from '../../_services/auth.service';
import { TokenStorageService } from '../../_services/token-storage.service';
@Component({
  selector: 'app-listevent',
  templateUrl: './listevent.component.html',
  styleUrls: ['./listevent.component.css']
})
export class ListeventComponent implements OnInit {
  Events: Observable<Event[]>;
  
  constructor(private userService: UserService,
    private router: Router,private token: TokenStorageService) {}
    currentUser: any;
    a:any;
  ngOnInit() {
    this.reloadData();
    this.currentUser = this.token.getUser();

  }

  reloadData() {
    this.Events = this.userService.getEvents();
  }

 
  
 
}
