import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/_services/token-storage.service';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/user.service';
import { Router } from '@angular/router';
import { User } from 'src/app/user';

@Component({
  selector: 'app-mesevents',
  templateUrl: './mesevents.component.html',
  styleUrls: ['./mesevents.component.css']
})
export class MeseventsComponent implements OnInit {
  currentUser: any;
  Events: Observable<Event[]>;
  Events1: Observable<Event[]>;
 // User: Observable<User[]>;

  constructor(private token: TokenStorageService,private userService: UserService,
    private router: Router) { }

  ngOnInit(): void {
    this.currentUser = this.token.getUser();
    this.reloadData();
  }
  reloadData(){  
    this.Events = this.userService.getEventsC(this.currentUser.id);
    this.Events1 = this.userService.getEventsP(this.currentUser.id);
   // this.User = this.userService.getEventsP(this.currentUser.id).;

  }
  eventDetailsP(id: number){
    this.router.navigate(['eventdetail',id]);
  }
  eventDetailsC(id: number){
    this.router.navigate(['eventdetailcreateur',id]);
  }
}
