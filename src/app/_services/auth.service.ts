import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const AUTH_API = 'http://localhost:8080/api/auth/';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(credentials): Observable<any> {
    return this.http.post(AUTH_API + 'signin', {
      username: credentials.username,
      password: credentials.password
    }, httpOptions);
  }
  alluser(): Observable<any> {
    return this.http.get(AUTH_API + 'allusers',httpOptions);
  }

  register(user): Observable<any> {
    return this.http.post(AUTH_API + 'signup', {
      username: user.username,
      nom:user.nom,
      prenom:user.prenom,
      dateNaissance:user.dateNaissance,
      tel:user.tel,
      email: user.email,
      password: user.password
    }, httpOptions);
  }
  orgevent(event): Observable<any> {
    return this.http.post(AUTH_API +'event/events', {
      adresse: event.adresse,

      sport: event.sport,
      prix:event.prix,
      nbplace:event.nbplace,
      niveau: event.niveau,
      heure_debut:event.heure_debut,
      date: event.date,
      duree:event.duree,
      id_createur:event.id_createur
    }, httpOptions);
  }  
  send(details): Observable<any> {
    return this.http.post('http://localhost:8080/api/auth/feed', {
      name: details.name,
      email: details.email,
      subject: details.subject,
      feedback:details.feedback

    }, httpOptions);
  }
}
